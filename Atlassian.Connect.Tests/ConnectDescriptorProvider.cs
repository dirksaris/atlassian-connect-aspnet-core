﻿using Atlassian.Connect.Models;

namespace Atlassian.Connect.Tests
{
	class ConnectDescriptorProvider : IConnectDescriptorProvider
	{
		public ConnectDescriptor GetConnectDescriptor()
		{
			return new ConnectDescriptor()
			{
				Name = "Test Addon",
				Authentication = new Authentication()
				{
					Type = AuthenticationType.Jwt
				}
			};
		}
	}
}
