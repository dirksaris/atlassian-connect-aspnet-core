using System; 
using Atlassian.Connect.Internal;
using NUnit.Framework;

namespace Atlassian.Connect.Tests
{
	[TestFixture]
	public class UnixTimestampTests
	{
		[Test]
		public void SpecificDateValidation()
		{
			Assert.That(new DateTime(2010, 10, 10, 10, 10, 00, DateTimeKind.Utc).AsUnixTimestampSeconds(), Is.EqualTo(1286705400L));
			Assert.That(new DateTime(2015, 05, 14, 07, 27, 23, DateTimeKind.Utc).AsUnixTimestampSeconds(), Is.EqualTo(1431588443));
		}
	}
}