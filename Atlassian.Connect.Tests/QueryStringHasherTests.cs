﻿using Atlassian.Connect.Internal;
using NUnit.Framework;

namespace Atlassian.Connect.Tests
{
	[TestFixture]
	public class QueryStringHasherTests
	{
		readonly QueryStringHasher _hasher = new QueryStringHasher();

		[Test]
		public void SimpleGetAtRoot()
		{
			Assert.That(_hasher.GenerateCanonicalRequest("GET", "/"), Is.EqualTo("GET&/&"));    
		}

		[Test]
		public void SimpleLowerCaseGetAtRoot()
		{
			Assert.That(_hasher.GenerateCanonicalRequest("get", "/"), Is.EqualTo("GET&/&"));
		}

		[Test]
		public void SimplePostWithAParameter()
		{
			Assert.That(_hasher.GenerateCanonicalRequest("POST", "/api/method", "one=two"), Is.EqualTo("POST&/api/method&one=two"));
		}

		[Test]
		public void SimpleGetWithParams()
		{
			Assert.That(_hasher.GenerateCanonicalRequest("get", "/and/more", "foo=bah"), Is.EqualTo("GET&/and/more&foo=bah"));
		}

		[Test]
		public void PostWithDuplicatedParams()
		{
			Assert.That(_hasher.GenerateCanonicalRequest("POST", "/simple", "foo=humbug&foo=bah"), Is.EqualTo("POST&/simple&foo=bah,humbug"));
		}

		[Test]
		public void SimpleGetWithManyParams()
		{
			Assert.That(_hasher.GenerateCanonicalRequest("GET", "/and/more", "last=param 1&foo=humbug&foo=bah&first=param1&last=param 2"),
				Is.EqualTo("GET&/and/more&first=param1&foo=bah,humbug&last=param%201,param%202"));
		}

		[Test]
		public void HandlesRequestWithJql()
		{
			Assert.That(_hasher.GenerateCanonicalRequest("GET", "/search", "jql=project = DOH"),
				Is.EqualTo("GET&/search&jql=project%20%3D%20DOH"));
		}
	}
}
