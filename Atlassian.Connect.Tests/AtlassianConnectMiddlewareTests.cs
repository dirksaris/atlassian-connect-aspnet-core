﻿using Atlassian.Connect.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Atlassian.Connect.Tests
{
	[TestFixture]
	public class AtlassianConnectMiddlewareTests
	{
		private readonly Task _onNextResult = Task.FromResult(0);
		private readonly RequestDelegate _onNext;

		public AtlassianConnectMiddlewareTests()
		{
			_onNext = _ =>
			{
				return _onNextResult;
			};
		}

		private IInstalledInstancePersister GetMockPersister()
		{
			return new Mock<IInstalledInstancePersister>().Object;
		}

		private IHttpContextAccessor GetHttpContext()
		{
			var accessorMock = new Mock<IHttpContextAccessor>();

			var httpContext = new DefaultHttpContext();

			httpContext.Request.Host = new HostString("localhost");
			httpContext.Request.Scheme = "https";

			accessorMock.SetupGet(m => m.HttpContext).Returns(httpContext);

			return accessorMock.Object;
		}

		private ConnectDescriptorAccessor GetMockDescriptorProvider()
		{
			return new ConnectDescriptorAccessor(GetOptions(), GetHttpContext(), new ConnectDescriptorProvider());
		}

		private AtlassianConnectOptions GetOptions()
		{
			var options = new AtlassianConnectOptions()
			{
				AddOnKey = "test.addon.key",
				InstallCallbackPath = "/installed",
				DescriptorPath = "/atlassian-connect.json"
			};
			return options;
		}

		private AtlassianConnectMiddleware GetMiddleware()
		{
			var persister = new InMemoryInstalledInstancePersister();
			var options = GetOptions();
			return new AtlassianConnectMiddleware(_onNext, new LoggerFactory().CreateLogger<AtlassianConnectMiddleware>(), options);
		}

		[Test]
		public async Task InstalledCallbackTestAsync()
		{
			var middleware = GetMiddleware();

			var context = new DefaultHttpContext();

			var installedInstance = new InstalledInstance()
			{
				Id = 0,
				BaseUrl = "http://localhost",
				ClientKey = "a",
				SharedSecret = "b"
			};

			var installedInstanceJson = JsonConvert.SerializeObject(installedInstance);
			byte[] byteArray = Encoding.UTF8.GetBytes(installedInstanceJson);
			MemoryStream stream = new MemoryStream(byteArray);

			context.Request.Body = stream;
			context.Request.Path = "/installed";
			context.Request.Method = "POST";

			await middleware.Invoke(context, GetMockPersister(), GetMockDescriptorProvider());

			Assert.AreEqual(context.Response.StatusCode, 201);
			stream.Dispose();
		}

		[Test]
		public async Task InstalledCallback_EmptyStream()
		{
			var middleware = GetMiddleware();

			var context = new DefaultHttpContext();

			context.Request.Path = "/installed";
			context.Request.Method = "POST";

			await middleware.Invoke(context, GetMockPersister(), GetMockDescriptorProvider());
			Assert.AreEqual(context.Response.StatusCode, 400);
		}

		[Test]
		public async Task InstalledCallback_GarbageStream()
		{
			var middleware = GetMiddleware();

			var context = new DefaultHttpContext();

			byte[] byteArray = Encoding.UTF8.GetBytes("this is a string of unparsable garbage");
			MemoryStream stream = new MemoryStream(byteArray);

			context.Request.Body = stream;
			context.Request.Path = "/installed";
			context.Request.Method = "POST";

			await middleware.Invoke(context, GetMockPersister(), GetMockDescriptorProvider());
			Assert.AreEqual(context.Response.StatusCode, 400);

			stream.Dispose();
		}

		[Test]
		public async Task InstalledCallback_WrongMethod()
		{
			var middleware = GetMiddleware();

			var context = new DefaultHttpContext();

			context.Request.Path = "/installed";
			context.Request.Method = "GET";

			await middleware.Invoke(context, GetMockPersister(), GetMockDescriptorProvider());
			Assert.AreEqual(context.Response.StatusCode, 405);
		}

		[Test]
		public async Task DescriptorEndpointTest()
		{
			var middleware = GetMiddleware();

			var accessorMock = new Mock<IHttpContextAccessor>();

			var httpContext = new DefaultHttpContext();
			httpContext.Request.Host = new HostString("localhost");
			httpContext.Request.Scheme = "https";
			httpContext.Request.Path = "/atlassian-connect.json";
			httpContext.Request.Method = "GET";
			httpContext.Response.Body = new MemoryStream();

			accessorMock.SetupGet(m => m.HttpContext).Returns(httpContext);

			await middleware.Invoke(httpContext, GetMockPersister(), GetMockDescriptorProvider()).ConfigureAwait(false);

			httpContext.Response.Body.Seek(0, SeekOrigin.Begin);

			Assert.AreEqual(httpContext.Response.StatusCode, 200);

			string bodyContents = null;
			using (var reader = new StreamReader(httpContext.Response.Body))
			{
				bodyContents = reader.ReadToEnd();
			}

			Assert.NotNull(bodyContents);

			var returnedDescriptor = ConnectDescriptor.ConvertFromJson(bodyContents);

			Assert.NotNull(returnedDescriptor);

			Assert.True(returnedDescriptor.Validate());
		}
	}
}
