﻿using NJsonSchema;
using NJsonSchema.CodeGeneration;
using System.Linq;

namespace Atlassian.Connect.Models.Generator
{
	/// <summary>The default enumeration name generator.</summary>
	public class DefaultEnumNameGenerator : IEnumNameGenerator
	{
		/// <summary>Generates the enumeration name/key of the given enumeration entry.</summary>
		/// <param name="index">The index of the enumeration value (check <see cref="JsonSchema4.Enumeration" /> and <see cref="JsonSchema4.EnumerationNames" />).</param>
		/// <param name="name">The name/key.</param>
		/// <param name="value">The value.</param>
		/// <param name="schema">The schema.</param>
		/// <returns>The enumeration name.</returns>
		public string Generate(int index, string name, object value, JsonSchema4 schema)
		{
			if (name.StartsWith("_-"))
			{
				name = "__" + name.Substring(2);
			}

			if (name.All(c => char.IsUpper(c)))
			{
				name = name.ToLower().Replace(name[0], char.ToUpper(name[0]));
			}

			return ConversionUtilities.ConvertToUpperCamelCase(name
				.Replace(":", "-").Replace(@"""", @""), true)
				.Replace(".", "_")
				.Replace("#", "_")
				.Replace("-", "_")
				.Replace("\\", "_");
		}
	}
}
