﻿using NJsonSchema;
using NJsonSchema.CodeGeneration.CSharp;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Atlassian.Connect.Models.Generator
{
	class Program
	{
		static async Task Main(string[] args)
		{
			// Read JSON Schema
			var schemaJson = await File.ReadAllTextAsync(Path.Combine(Directory.GetCurrentDirectory(), "ConfluenceGlobalSchema.json"));
			var schema = await JsonSchema4.FromJsonAsync(schemaJson);
			var enums = schema.Definitions.Where(kvp => kvp.Value.IsEnumeration);
			var settings = new CSharpGeneratorSettings()
			{
				Namespace = "Atlassian.Connect.Models",
				ClassStyle = CSharpClassStyle.Poco,
				ArrayBaseType = "IEnumerable",
				ArrayType = "IEnumerable",
				ArrayInstanceType = "List",
				EnumNameGenerator = new DefaultEnumNameGenerator()
			};
			var generator = new CSharpGenerator(schema, settings);

			// Write output
			var file = generator.GenerateFile();
			using (var writer = File.CreateText("../../../../Atlassian.Connect.Models/Output.cs"))
			{
				writer.Write(file);
			}

			Console.WriteLine("Models Generated from Json Schema");
		}
	}
}
