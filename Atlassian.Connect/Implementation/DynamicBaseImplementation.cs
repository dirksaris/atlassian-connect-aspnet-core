using System.Collections.Generic;
using System.Dynamic;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Atlassian.Connect.Implementation
{
	public class DynamicBaseImplementation : DynamicObject
	{
		readonly Dictionary<string, object> _dictionary = new Dictionary<string, object>();

		[JsonIgnore]
		public int Count
		{
			get { return _dictionary.Count; }
		}

		public override bool TryGetMember(GetMemberBinder binder, out object result)
		{
			return _dictionary.TryGetValue(binder.Name, out result);
		}

		public override bool TrySetMember(SetMemberBinder binder, object value)
		{
			_dictionary[binder.Name] = value;
			return true;
		}

		public string ToJson()
		{
			return JsonConvert.SerializeObject(this, new JsonSerializerSettings()
			{
				ContractResolver = new CamelCasePropertyNamesContractResolver()
			});
		}
	}
}