﻿using Microsoft.EntityFrameworkCore;

namespace Atlassian.Connect.Entities
{
	public class InstancesContext : DbContext
	{
		// TODO specify connection string
		public InstancesContext() : base()
		{
		}

		public DbSet<IInstalledInstance> InstalledInstances { get; set; }
	}
}