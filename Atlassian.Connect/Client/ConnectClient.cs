﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Atlassian.Connect.Internal;
using Microsoft.AspNetCore.Http;

namespace Atlassian.Connect
{
	public class ConnectClient : HttpClient
	{
		public ConnectClient(IHttpContextAccessor httpContextAccessor, JwtInHttpClientMessageHandler handler) : 
			base(handler)
		{
			var request = httpContextAccessor.HttpContext.Request;

			var baseUrl = request.Query["xdm_e"].ToString() + request.Query["cp"].ToString();
			if (!string.IsNullOrEmpty(baseUrl))
			{
				// normalize if there's a slash
				baseUrl = baseUrl.TrimEnd('/') + "/";
				base.BaseAddress = new Uri(baseUrl);
			}
		}
	}
}
