﻿using Atlassian.Connect.Jwt;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Atlassian.Connect.Internal
{
	public class JwtInHttpClientMessageHandler : DelegatingHandler
	{
		private readonly IHttpContextAccessor _httpContextAccessor;
		private readonly HttpRequest _request;
		private static readonly QueryStringHasher Hasher = new QueryStringHasher();
		private readonly IInstalledInstancePersister _persister;
		private readonly AtlassianConnectOptions _options;
		private readonly ILogger _logger;

		public JwtInHttpClientMessageHandler(IHttpContextAccessor contextAccessor, IInstalledInstancePersister persister, AtlassianConnectOptions options, ILogger<JwtInHttpClientMessageHandler> logger)
		{
			_httpContextAccessor = contextAccessor;
			_request = contextAccessor.HttpContext.Request;
			InnerHandler = new HttpClientHandler();
			_persister = persister;
			_options = options;
			_logger = logger;
		}

		protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
		{
			var path = request.RequestUri.AbsolutePath;
			if (!String.IsNullOrEmpty(_request.Query["cp"].ToString()))
			{
				path = path.Replace(_request.Query["cp"].ToString(), "");
			}
			if (!path.StartsWith("/"))
				path = "/" + path;

			var qsh = Hasher.CalculateHash(request.Method.ToString(), path,
				request.RequestUri.Query.TrimStart('?'));

			var installedInstance = await _persister.GetFromClientKey(_request.GetClientKey());

			var tokenBuilder = new DecodedJwtToken(installedInstance.SharedSecret);
			tokenBuilder.Claims.Add("iss", _options.AddOnKey);
			tokenBuilder.Claims.Add("iat", DateTime.UtcNow.AsUnixTimestampSeconds());
			tokenBuilder.Claims.Add("exp", DateTime.UtcNow.AddSeconds(30).AsUnixTimestampSeconds());
			tokenBuilder.Claims.Add("qsh", qsh);
			tokenBuilder.Claims.Add("aud", new[] { installedInstance.ClientKey });

			var encodedToken = tokenBuilder.Encode();
			string jwt = encodedToken.JwtTokenString;

			request.Headers.Add("Authorization", "JWT " + jwt);
			request.Headers.Add("User-Agent", "atlassian-connect-aspnet-core/0.0.1");

			return await base.SendAsync(request, cancellationToken);
		}
	}
}