﻿using System.Collections.Generic;
using System.Text;

namespace Atlassian.Connect
{
	public class SchemaValidationMessageCollection : List<string>
	{
		public SchemaValidationMessageCollection() : base()
		{ }

		public override string ToString()
		{
			var sb = new StringBuilder();

			for (int i = 0; i < this.Count; i++)
			{
				sb.Append($"Message {i}: {this[i]}");
				if (i != this.Count - 1)
				{
					sb.Append(" | ");
				}
			}

			return sb.ToString();
		}



	}
}