﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Atlassian.Connect.Entities;

namespace Atlassian.Connect
{
	public interface IInstalledInstancePersister
	{
		Task SaveAsync(IInstalledInstance installedInstance);

		Task<IInstalledInstance> GetFromClientKey(string clientKey);
	}
}
