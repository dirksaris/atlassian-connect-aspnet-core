﻿using System;
using System.Collections.Generic;
using System.Text;
using Atlassian.Connect.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace Atlassian.Connect
{
	public class InMemoryInstalledInstancePersister : IInstalledInstancePersister
	{
		private readonly IList<IInstalledInstance> _instances;

		public InMemoryInstalledInstancePersister()
		{
			_instances = new List<IInstalledInstance>();
		}

		public Task SaveAsync(IInstalledInstance instance)
		{
			_instances.Add(instance);
			return Task.CompletedTask;
		}

		public Task<IInstalledInstance> GetFromClientKey(string clientKey)
		{
			return Task.FromResult(_instances.FirstOrDefault(i => i.ClientKey == clientKey));
		}
	}
}
