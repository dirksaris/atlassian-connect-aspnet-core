﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web;
using Atlassian.Connect.Entities;
using Atlassian.Connect.Internal;
using JWT;
using Microsoft.AspNetCore.Http;
using System.Linq;
using JWT.Serializers;
using JWT.Algorithms;
using JWT.Builder;

namespace Atlassian.Connect
{
	public static class RequestExtensions
	{
		public static string GetClientKey(this HttpRequest request)
		{
			IJsonSerializer serializer = new JsonNetSerializer();
			IDateTimeProvider provider = new UtcDateTimeProvider();
			IJwtValidator validator = new JwtValidator(serializer, provider);
			IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
			IJwtDecoder decoder = new JwtDecoder(serializer, validator, urlEncoder);

			var jwt = request.GetJwtToken();

			var claims = decoder.DecodeToObject(jwt);

			return claims["iss"] as string;
		}

		public static string GetJwtToken(this HttpRequest request)
		{
			var requestToken = request.Query["jwt"].ToString();
			if (String.IsNullOrEmpty(requestToken))
			{
				requestToken = request.Headers["Authorization"].ToString();
				if (!String.IsNullOrEmpty(requestToken))
				{
					requestToken = requestToken.Replace("JWT ", "");
				}
			}
			return requestToken;
		}

		public static string BaseUrl(this HttpRequest request)
		{
			return GetProtocol(request) +
				"://" +
				GetHost(request) +
				request.PathBase.ToString().TrimEnd('/') + "/";
		}

		public static string GetHost(HttpRequest request)
		{
			if (!string.IsNullOrWhiteSpace(request.Headers["X-Original-Host"].ToString()))
			{
				return request.Headers["X-Original-Host"];
			}
			return request.Host.ToString();
		}

		public static string GetProtocol(HttpRequest request)
		{
			if (!string.IsNullOrWhiteSpace(request.Headers["X-Forwarded-Proto"].ToString()))
			{
				return request.Headers["X-Forwarded-Proto"];
			}
			return request.Scheme;
		}
	}
}