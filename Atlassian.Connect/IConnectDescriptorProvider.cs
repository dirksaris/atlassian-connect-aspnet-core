﻿namespace Atlassian.Connect
{
	public interface IConnectDescriptorProvider
	{
		ConnectDescriptor GetConnectDescriptor();
	}
}