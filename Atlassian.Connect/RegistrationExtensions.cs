﻿using Atlassian.Connect.Internal;
using Atlassian.Connect.Jwt;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;

namespace Atlassian.Connect
{
	public static class RegistrationExtensions
	{
		public static AtlassianConnectBuilder AddAtlassianConnectBuilder(this IServiceCollection services)
		{
			return new AtlassianConnectBuilder(services);
		}

		public static AtlassianConnectBuilder AddAtlassianConnect<T>(this IServiceCollection services, Action<AtlassianConnectOptions> options) where T : IConnectDescriptorProvider
		{
			var builder = new AtlassianConnectBuilder(services);

			builder.Services.Configure(options);

			builder.Services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
			builder.Services.AddHttpContextAccessor();
			builder.Services.AddOptions();
			builder.Services.AddSingleton(
				resolver => resolver.GetRequiredService<IOptions<AtlassianConnectOptions>>().Value);
			builder.Services.AddSingleton(typeof(IConnectDescriptorProvider), typeof(T));
			builder.Services.AddScoped<JwtAuthenticationFilterInternal>();
			builder.Services.AddTransient<JwtInHttpClientMessageHandler>();
			builder.Services.AddTransient<ConnectClient>();
			builder.Services.AddSingleton<ConnectDescriptorAccessor>();

			return builder;
		}

		public static IApplicationBuilder UseAtlassianConnect(this IApplicationBuilder app)
		{
			var loggerFactory = app.ApplicationServices.GetService(typeof(ILoggerFactory)) as ILoggerFactory;
			if (loggerFactory == null) throw new ArgumentNullException(nameof(loggerFactory));
			var logger = loggerFactory.CreateLogger("AtlassianConnect.Startup");

			var scopeFactory = app.ApplicationServices.GetService<IServiceScopeFactory>();
			using (var scope = scopeFactory.CreateScope())
			{
				var serviceProvider = scope.ServiceProvider;

				var persistedGrants = serviceProvider.GetService(typeof(IInstalledInstancePersister));
				if (persistedGrants.GetType().FullName == typeof(InMemoryInstalledInstancePersister).FullName)
				{
					logger.LogInformation("In Memory Installed Instance Persister in use.");
				}
			}

			app.UseMiddleware<AtlassianConnectMiddleware>();

			return app;
		}

		public static AtlassianConnectBuilder AddInMemoryPersister(this AtlassianConnectBuilder builder)
		{
			builder.Services.AddSingleton<IInstalledInstancePersister, InMemoryInstalledInstancePersister>();
			return builder;
		}
	}
}
