﻿namespace Atlassian.Connect
{
	public class AtlassianConnectOptions
	{
		public string InstallCallbackPath { get; set; }
		public string AddOnKey { get; set; }
		public string DescriptorPath { get; set; }
		public bool ValidateDescriptorSchema { get; set; } = true;
	}
}
