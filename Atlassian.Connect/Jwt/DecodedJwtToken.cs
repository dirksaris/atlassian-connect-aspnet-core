using System;
using System.Collections.Generic;
using System.Web;
using Atlassian.Connect.Internal;
using JWT;
using JWT.Algorithms;
using JWT.Builder;
using JWT.Serializers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;

namespace Atlassian.Connect.Jwt
{
	public class DecodedJwtToken
	{
		private static readonly QueryStringHasher Hasher = new QueryStringHasher();

		public DecodedJwtToken(string sharedSecret)
		{
			SharedSecret = sharedSecret;
			Claims = new Dictionary<string, object>();
		}

		public string SharedSecret { get; set; }
		public IDictionary<string, object> Claims { get; set; }

		public EncodedJwtToken Encode()
		{
			IJwtEncoder encoder = new JwtEncoder(new HMACSHA256Algorithm(), new JsonNetSerializer(), new JwtBase64UrlEncoder());

			string token = encoder.Encode(Claims, SharedSecret);

			return new EncodedJwtToken(SharedSecret, token);
		}

		public void ValidateToken(HttpRequest request)
		{
			ValidateTokenHasNotExpired();
			ValidateQueryStringHash(request);
		}

		private void ValidateTokenHasNotExpired()
		{
			var expiresAt = Convert.ToInt64(Claims["exp"]);

			var now = DateTime.UtcNow.AsUnixTimestampSeconds();

			if (expiresAt < now)
				throw new Exception("JWT Token Expired");
		}

		private void ValidateQueryStringHash(HttpRequest request)
		{
			var path = UriHelper.BuildRelative(path: request.Path);

			if (!path.StartsWith("/"))
				path = "/" + path;

			var qsh = Hasher.CalculateHash(request.Method, path, request.QueryString.ToString().TrimStart('?'));
			var requestQsh = Claims["qsh"] as string;

			if (qsh != requestQsh)
				throw new Exception("QSH Does not match.");
		}
	}
}