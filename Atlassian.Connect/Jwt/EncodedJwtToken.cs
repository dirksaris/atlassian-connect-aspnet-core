﻿using System.Collections.Generic;
using JWT;
using JWT.Serializers;

namespace Atlassian.Connect.Jwt
{
	public class EncodedJwtToken
	{
		public EncodedJwtToken(string sharedSecret, string jwtToken)
		{
			SharedSecret = sharedSecret;
			JwtTokenString = jwtToken;
		}

		public string SharedSecret { get; set; }
		public string JwtTokenString { get; set; }

		public DecodedJwtToken Decode()
		{
			IJsonSerializer serializer = new JsonNetSerializer();
			IDateTimeProvider provider = new UtcDateTimeProvider();
			IJwtValidator validator = new JwtValidator(serializer, provider);
			IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();

			IJwtDecoder decoder = new JwtDecoder(serializer, validator, urlEncoder);

			return new DecodedJwtToken(SharedSecret)
			{
				Claims = decoder.DecodeToObject(JwtTokenString, SharedSecret, true)
			};
		}
	}
}
