﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc;

namespace Atlassian.Connect.Jwt
{
	public class RequireAtlassianConnectJWTAttribute : ServiceFilterAttribute
	{
		public RequireAtlassianConnectJWTAttribute() : base(typeof(JwtAuthenticationFilterInternal)) { }
	}
}
