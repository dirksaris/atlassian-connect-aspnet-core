﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;

namespace Atlassian.Connect
{
	public class AtlassianConnectBuilder
	{

		public AtlassianConnectBuilder(IServiceCollection services)
		{
			Services = services ?? throw new ArgumentNullException(nameof(services));
		}

		public IServiceCollection Services { get; }

	}
}
