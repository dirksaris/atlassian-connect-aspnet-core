﻿using Atlassian.Connect.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Atlassian.Connect
{
	public class AtlassianConnectMiddleware //: IMiddleware
	{
		private readonly RequestDelegate _next;
		private readonly ILogger _logger;
		private readonly AtlassianConnectOptions _options;

		public AtlassianConnectMiddleware(RequestDelegate next, ILogger<AtlassianConnectMiddleware> logger, AtlassianConnectOptions options)
		{
			_next = next;
			_logger = logger;
			_options = options;
		}

		public async Task Invoke(HttpContext context, IInstalledInstancePersister persister, ConnectDescriptorAccessor descriptorProvider)
		{
			if (context == null) throw new ArgumentNullException(nameof(context));

			// Handle installed path
			if (context.Request.Path.Equals(_options.InstallCallbackPath, StringComparison.OrdinalIgnoreCase))
			{
				await HandleInstalledPostAsync(context, persister);
				return;
			}
			// Handle descriptor path
			if (context.Request.Path.Equals(_options.DescriptorPath, StringComparison.OrdinalIgnoreCase))
			{
				await HandleDescriptorGetAsync(context, persister, descriptorProvider);
				return;
			}

			await _next(context);
		}

		public async Task HandleDescriptorGetAsync(HttpContext context, IInstalledInstancePersister persister, ConnectDescriptorAccessor descriptorProvider)
		{
			if (!HttpMethods.IsGet(context.Request.Method))
			{
				context.Response.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
				return;
			}

			var descriptor = descriptorProvider.GetDescriptor(_options.ValidateDescriptorSchema);

			string json = descriptor.ToJson();

			context.Response.StatusCode = (int)HttpStatusCode.OK;
			await context.Response.WriteAsync(json);
		}

		public async Task HandleInstalledPostAsync(HttpContext context, IInstalledInstancePersister persister)
		{
			if (!HttpMethods.IsPost(context.Request.Method))
			{
				context.Response.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
				return;
			}

			string bodyText;
			using (StreamReader reader = new StreamReader(context.Request.Body, Encoding.UTF8))
			{
				bodyText = await reader.ReadToEndAsync();
			}

			IInstalledInstance instanceData = null;
			try
			{
				instanceData = JsonConvert.DeserializeObject<InstalledInstance>(bodyText);
			}
			catch (Exception ex)
			{
				context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
			}

			if (instanceData != null)
			{
				await persister.SaveAsync(instanceData);
				context.Response.StatusCode = (int)HttpStatusCode.Created;
			}
			else
			{
				context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
			}
		}
	}
}
