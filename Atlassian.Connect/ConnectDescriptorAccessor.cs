﻿using Atlassian.Connect.Models;
using Microsoft.AspNetCore.Http;

namespace Atlassian.Connect
{
	public class ConnectDescriptorAccessor
	{
		private readonly AtlassianConnectOptions _options;
		private readonly IHttpContextAccessor _contextAccessor;
		private readonly IConnectDescriptorProvider _descriptorProvider;

		public ConnectDescriptorAccessor(AtlassianConnectOptions options, IHttpContextAccessor contextAccessor, IConnectDescriptorProvider descriptorProvider)
		{
			_options = options;
			_contextAccessor = contextAccessor;
			_descriptorProvider = descriptorProvider;
		}

		public virtual ConnectDescriptor GetDescriptor(bool validate = false)
		{
			var descriptorInternal = _descriptorProvider.GetConnectDescriptor();

			string baseUrl = _contextAccessor?.HttpContext?.Request?.BaseUrl();
			descriptorInternal.BaseUrl = new System.Uri(baseUrl ?? "http://localhost");

			// Set install callback path
			var lifecycle = descriptorInternal.Lifecycle ?? new Lifecycle();
			lifecycle.Installed = $"/{_options.InstallCallbackPath.TrimStart('/')}";
			descriptorInternal.Lifecycle = lifecycle;

			// Set addon key
			if (string.IsNullOrWhiteSpace(descriptorInternal.Key))
			{
				descriptorInternal.Key = _options.AddOnKey;
			}

			descriptorInternal.Validate(validate);

			return descriptorInternal;
		}
	}
}
