﻿namespace Atlassian.Connect.AutoRegister
{
	public class AtlassianUpmClientOptions
	{
		public AtlassianUpmClientOptions() { }

		public AtlassianUpmClientOptions(string host, string product, string username, string password)
		{
			Host = host;
			Product = product;
			Username = username;
			Password = password;
		}

		public string Host { get; set; }
		public string Product { get; set; }
		public string Username { get; set; }
		public string Password { get; set; }

	}
}