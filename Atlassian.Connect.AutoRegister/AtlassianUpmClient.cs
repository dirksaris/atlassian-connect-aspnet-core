﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NgrokAspNetCore;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Atlassian.Connect.AutoRegister
{
	public class AtlassianUpmClient
	{
		private readonly HttpClient _httpClient;
		private readonly Uri _baseUri;
		private readonly ILogger _logger;

		public AtlassianUpmClient(HttpClient httpClient, IOptionsMonitor<AtlassianUpmClientOptions> optionsMonitor, NgrokLocalApiClient ngrokClient, ILogger<AtlassianUpmClient> logger)
		{
			var options = optionsMonitor.CurrentValue;
			if (string.IsNullOrWhiteSpace(options.Host)) throw new ArgumentNullException("host");
			if (string.IsNullOrWhiteSpace(options.Product)) throw new ArgumentNullException("product");
			if (string.IsNullOrWhiteSpace(options.Username)) throw new ArgumentNullException("username");
			if (string.IsNullOrWhiteSpace(options.Password)) throw new ArgumentNullException("password");

			var uriBuilder = new UriBuilder("https", options.Host, 443);
			if (options.Product.ToLower() == "confluence")
			{
				uriBuilder.Path = "wiki";
			}
			else if (options.Product.ToLower() == "jira")
			{
				uriBuilder.Path = "jira";
			}

			uriBuilder.Path = $"{uriBuilder.Path}/rest/plugins/1.0/";

			_baseUri = uriBuilder.Uri;

			_httpClient = httpClient;
			_httpClient.BaseAddress = uriBuilder.Uri;
			var headerString = $"{options.Username}:{options.Password}";
			headerString = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(headerString));

			_httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", headerString);

			_logger = logger;
		}

		public async Task<string> GetUpmToken()
		{
			var response = await _httpClient.GetAsync(_baseUri);
			if (response.IsSuccessStatusCode)
			{
				if (response.Headers.TryGetValues("upm-token", out var upmTokenValues))
				{
					var token = upmTokenValues.First();
					return token;
				}
			}
			return null;
		}

		public async Task<UpmRegistrationResult> InstallPlugin(string pluginName, string pluginUri)
		{
			var upmToken = await GetUpmToken();
			var request = new HttpRequestMessage(HttpMethod.Post, $"{_baseUri}?token={upmToken}");
			var body = new { pluginUri = pluginUri, pluginName = pluginName };
			request.Content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/vnd.atl.plugins.install.uri+json");
			request.Headers.Add("Accept", "application/json");

			var response = await _httpClient.SendAsync(request);

			if (!response.IsSuccessStatusCode)
			{
				return UpmRegistrationResult.Fail($"Failed to post add-on metadata to upm");
			}

			var json = await response.Content.ReadAsStringAsync();
			var responseContent = JsonConvert.DeserializeObject(json) as JObject;
			var regOpLink = (string)(responseContent?.SelectToken("$.links.self") ?? string.Empty);

			if (string.IsNullOrEmpty(regOpLink))
			{
				return UpmRegistrationResult.Fail("upm installation status link missing from upm install status result");
			}

			// Wait and recurse for install status to be updated
			var result = await WaitForResult(regOpLink);

			return result;
		}

		public async Task<UpmRegistrationResult> WaitForResult(string operationLink, CancellationToken cancellationToken = default(CancellationToken))
		{
			// Default to 30 second cancellation
			if (cancellationToken == default(CancellationToken))
			{
				var tokenSource = new CancellationTokenSource(30000);
				cancellationToken = tokenSource.Token;
			}

			// Return failure if canceled (usually timeout reached)
			if (cancellationToken.IsCancellationRequested)
			{
				return UpmRegistrationResult.Fail("Timeout reached");
			}

			try
			{
				// Send request to get operation status
				var request = new HttpRequestMessage(HttpMethod.Get, $"https://{_baseUri.Host}{operationLink}");
				var response = await _httpClient.SendAsync(request, cancellationToken);

				// Follow the redirect if there is one
				if (response.StatusCode == HttpStatusCode.Redirect || response.StatusCode == HttpStatusCode.RedirectMethod)
				{
					// Validate the redirect uri
					var redirectString = response.Headers.Location.OriginalString;
					if (Uri.TryCreate(redirectString, UriKind.Absolute, out Uri redirectUri) &&
						redirectUri.Host == _baseUri.Host &&
						redirectUri.Scheme == "https")
					{
						var redirectRequest = new HttpRequestMessage(HttpMethod.Get, redirectString);
						response = await _httpClient.SendAsync(redirectRequest);
					}
				}

				// If non 200 response code, return error
				if (!response.IsSuccessStatusCode)
				{
					return UpmRegistrationResult.Fail($"Error status code received: {response.StatusCode}");
				}

				// Parse response
				var json = await response.Content.ReadAsStringAsync();
				var responseContent = JsonConvert.DeserializeObject(json) as JObject;

				// Parse status.done in json. If status.done exists, install was a failure, so parse the error message field
				var isDone = (bool)(responseContent?.SelectToken("$.status.done", false) ?? false);
				if (isDone)
				{
					var errorMessage = (string)(responseContent?.SelectToken("$.status.errorMessage") ?? string.Empty);
					return UpmRegistrationResult.Fail(errorMessage);
				}

				// If the key exists, installation was successful. Return success unless plugin is disabled 
				var key = (string)(responseContent?.SelectToken("$.key") ?? string.Empty);
				if (!string.IsNullOrWhiteSpace(key))
				{
					var enabled = (bool)(responseContent?.SelectToken("$.enabled") ?? false);
					if (!enabled)
					{
						return UpmRegistrationResult.Fail("Add-on is disabled. Enable manually via NPM", key);
					}

					return UpmRegistrationResult.Success(key);
				}

				var pingAfter = (int)(responseContent?.SelectToken("$.pingAfter") ?? 1000);
				await Task.Delay(pingAfter, cancellationToken);
				return await WaitForResult(operationLink, cancellationToken);
			}
			catch (TaskCanceledException)
			{
				return UpmRegistrationResult.Fail("Timeout reached");
			}
			catch (Exception ex)
			{
				_logger.LogError("Exception thrown while polling for registration status", ex);
				return UpmRegistrationResult.Fail("Exception thrown while polling for registration status");
			}
		}

		public async Task<bool> UninstallPlugin(string pluginKey)
		{
			//var upmToken = await GetUpmToken();
			var request = new HttpRequestMessage(HttpMethod.Delete, $"{_baseUri}{pluginKey}-key");

			var response = await _httpClient.SendAsync(request);
			return response.IsSuccessStatusCode;
		}
	}
}
