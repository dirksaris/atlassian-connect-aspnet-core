﻿using NgrokAspNetCore;
using System.Linq;
using System.Threading.Tasks;

namespace Atlassian.Connect.AutoRegister
{
	public class RegistrationClient
	{
		private readonly AtlassianUpmClient _upmClient;
		private readonly NgrokLocalApiClient _ngrokClient;
		private string _addonKey;

		public RegistrationClient(AtlassianUpmClient upmClient, NgrokLocalApiClient ngrokClient)
		{
			_upmClient = upmClient;
			_ngrokClient = ngrokClient;
		}

		public string AddonKey => _addonKey;

		public async Task<UpmRegistrationResult> RegisterAddon(string addonName)
		{
			var url = (await _ngrokClient.GetTunnelListAsync()).Select(t => t.public_url)
				.First(t => t.StartsWith("https"));
			var result = await _upmClient.InstallPlugin(addonName, $"{url}/atlassian-connect.json");
			if (result.IsSuccess)
			{
				_addonKey = result.AddonKey;
			}
			return result;
		}

		public async Task<bool> UnregisterAddon(string addonKey)
		{
			if (string.IsNullOrWhiteSpace(addonKey))
			{
				return false;
			}

			var result = await _upmClient.UninstallPlugin(addonKey);
			return result;
		}
	}
}
