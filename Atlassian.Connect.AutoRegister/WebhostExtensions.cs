﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Server.Features;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NgrokAspNetCore;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Atlassian.Connect.AutoRegister
{
	public static class WebhostExtensions
	{
		public static void AddSelfRegistration(
			this IServiceCollection services,
			Action<AtlassianUpmClientOptions> options,
			NgrokOptions ngrokOptions = null)
		{
			services.AddNgrok(ngrokOptions);
			services.Configure<AtlassianUpmClientOptions>(options);
			services.AddHttpClient<AtlassianUpmClient>().ConfigurePrimaryHttpMessageHandler(() =>
			{
				return new HttpClientHandler
				{
					AllowAutoRedirect = false
				};
			});
			services.AddSingleton<RegistrationClient>();
		}
		public static async Task RegisterConnectAddon(this IWebHost host)
		{

			var loggerFactory = host.Services.GetService(typeof(ILoggerFactory)) as ILoggerFactory;
			if (loggerFactory == null) throw new ArgumentNullException(nameof(loggerFactory));
			var logger = loggerFactory.CreateLogger("AtlassianConnectSelfRegister");

			// Validate required config elements are present
			var optionsMonitor = host.Services.GetRequiredService<IOptionsMonitor<AtlassianUpmClientOptions>>();
			var options = optionsMonitor.CurrentValue;
			if (string.IsNullOrWhiteSpace(options.Host)) throw new ArgumentNullException("atlassian-connect:host");
			if (string.IsNullOrWhiteSpace(options.Product)) throw new ArgumentNullException("atlassian-connect:product");
			if (string.IsNullOrWhiteSpace(options.Username)) throw new ArgumentNullException("atlassian-connect:username");
			if (string.IsNullOrWhiteSpace(options.Password)) throw new ArgumentNullException("atlassian-connect:password");

			// Get addon name from descriptor
			string name;

			var descriptorProvider = host.Services.GetRequiredService<ConnectDescriptorAccessor>();
			var descriptor = descriptorProvider.GetDescriptor();
			name = descriptor.Name;

			//using (var scope = host.Services.CreateScope())
			//{
			//	var descriptorProvider = scope.ServiceProvider.GetRequiredService<ConnectDescriptorAccessor>();
			//	var descriptor = descriptorProvider.GetDescriptor();
			//	name = descriptor.Name;
			//}
			if (string.IsNullOrWhiteSpace(name))
			{
				throw new ArgumentNullException("Descriptor Name");
			}

			await host.StartNgrokAsync();
			logger.LogInformation("Started Ngrok");


			var registrationClient = host.Services.GetRequiredService<RegistrationClient>();
			logger.LogInformation("Registering Addon");
			var registrationResult = await registrationClient.RegisterAddon(name);
			if (registrationResult.IsSuccess)
			{
				logger.LogInformation("Addon Registered");
			}
			else
			{
				logger.LogInformation("Addon Registration failed. Message: {message}", registrationResult.Message);
			}

			// Register the UnregisterAddon method to fire when the entire app shuts down
			var applicationLifetime = host.Services.GetRequiredService<IApplicationLifetime>();
			applicationLifetime.ApplicationStopping.Register(async () => await registrationClient.UnregisterAddon(registrationResult.AddonKey));
		}

		private static async Task<NgrokOptions> ConfigureOptions(IWebHost host)
		{
			var services = host.Services;
			var options = services.GetRequiredService<NgrokOptions>();
			// Set address automatically if not provided or invalid
			var addresses = (host.ServerFeatures[typeof(IServerAddressesFeature)] as IServerAddressesFeature)?.Addresses;
			if (!string.IsNullOrWhiteSpace(options.ApplicationHttpUrl) || !Uri.TryCreate(options.ApplicationHttpUrl, UriKind.Absolute, out Uri uri))
			{
				options.ApplicationHttpUrl = addresses.FirstOrDefault(a => a.StartsWith("http://")) ?? addresses.FirstOrDefault();
			}

			// Ensure ngrok is installed and set path
			var ngrokDownloader = services.GetRequiredService<NgrokDownloader>();
			var ngrokFullPath = await ngrokDownloader.EnsureNgrokInstalled(options);
			options.NgrokPath = ngrokFullPath;

			return options;
		}
	}
}
