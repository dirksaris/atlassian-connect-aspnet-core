﻿namespace Atlassian.Connect.AutoRegister
{
	public class UpmRegistrationResult
	{
		public UpmRegistrationResult() { }
		public UpmRegistrationResult(bool isSuccess, string message = null, string addonKey = null)
		{
			IsSuccess = isSuccess;
			Message = message;
			AddonKey = addonKey;
		}

		public bool IsSuccess { get; set; }
		public string AddonKey { get; set; }
		public string Message { get; set; }

		public static UpmRegistrationResult Success(string addonKey)
		{
			return new UpmRegistrationResult(true, null, addonKey);
		}

		public static UpmRegistrationResult Fail(string message, string addonKey = null)
		{
			return new UpmRegistrationResult(false, message, addonKey);
		}
	}
}