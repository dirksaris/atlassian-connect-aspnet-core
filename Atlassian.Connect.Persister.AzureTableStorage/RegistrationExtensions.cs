﻿using Microsoft.Extensions.DependencyInjection;

namespace Atlassian.Connect.Persister.AzureTableStorage
{
	public static class RegistrationExtensions
	{
		public static AtlassianConnectBuilder AddAzureTableStoragePersister(
			this AtlassianConnectBuilder builder,
			string connectionString,
			string tableName = "AtlassianConnectInstalls")
		{
			builder.Services.AddSingleton<IInstalledInstancePersister, AzureTableStoragePersister>((serviceProvider) =>
				new AzureTableStoragePersister(connectionString, tableName));
			return builder;
		}
	}
}
