﻿using Atlassian.Connect.Entities;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System.Threading.Tasks;

namespace Atlassian.Connect.Persister.AzureTableStorage
{
	public class AzureTableStoragePersister : IInstalledInstancePersister
	{
		private readonly CloudTable _table;
		private bool _initialized;
		private static readonly string PartitionKey = "ConfluenceConnectInstalledInstance";

		public AzureTableStoragePersister(string connectionString, string tableName)
		{
			var storageAccount = CloudStorageAccount.Parse(connectionString);
			var tableClient = storageAccount.CreateCloudTableClient();
			var table = tableClient.GetTableReference(tableName);
			_table = table;
			_initialized = false;
		}

		public async Task Initialize()
		{
			if (!_initialized)
			{
				//await _table.CreateIfNotExistsAsync();
				_initialized = true;
			}
		}

		public async Task<IInstalledInstance> GetFromClientKey(string clientKey)
		{
			await Initialize();
			var retriveOperation = TableOperation.Retrieve<InstalledInstanceTableEntity>(PartitionKey, clientKey);

			var tableResult = await _table.ExecuteAsync(retriveOperation);
			if (tableResult.Result != null)
			{
				return tableResult.Result as IInstalledInstance;
			}
			return null;
		}

		public async Task SaveAsync(IInstalledInstance installedInstance)
		{
			await Initialize();
			var row = InstalledInstanceTableEntity.CreateFromInstance(installedInstance);
			row.SetTableKeys(PartitionKey);

			var insertOperation = TableOperation.InsertOrReplace(row);

			await _table.ExecuteAsync(insertOperation);
		}
	}
}
