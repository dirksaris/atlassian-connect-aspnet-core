﻿using Atlassian.Connect.Entities;
using Microsoft.WindowsAzure.Storage.Table;

namespace Atlassian.Connect.Persister.AzureTableStorage
{
	public class InstalledInstanceTableEntity : TableEntity, IInstalledInstance
	{
		public int Id { get; set; }
		public string BaseUrl { get; set; }
		public string ClientKey { get; set; }
		public string SharedSecret { get; set; }
		public void SetTableKeys(string partitionKey)
		{
			PartitionKey = partitionKey;
			RowKey = ClientKey;
		}

		public static InstalledInstanceTableEntity CreateFromInstance(IInstalledInstance inputInstance)
		{
			return new InstalledInstanceTableEntity()
			{
				Id = inputInstance.Id,
				BaseUrl = inputInstance.BaseUrl,
				ClientKey = inputInstance.ClientKey,
				SharedSecret = inputInstance.SharedSecret
			};
		}
	}
}
