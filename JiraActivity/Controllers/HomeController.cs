﻿using Atlassian.Connect;
using Atlassian.Connect.Jwt;
using JiraActivity.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Dynamic;

namespace JiraActivity.Controllers
{
	public class HomeController : Controller
	{
		private readonly IInstalledInstancePersister _installedInstancePersister;
		private readonly ConnectClient _connectClient;
		public HomeController(IInstalledInstancePersister installedInstancePersister, ConnectClient connectClient)
		{
			_installedInstancePersister = installedInstancePersister;
			_connectClient = connectClient;
		}

		public IActionResult Index()
		{
			return View();
		}

		public IActionResult About()
		{
			ViewData["Message"] = "Your application description page.";

			return View();
		}

		public IActionResult Contact()
		{
			ViewData["Message"] = "Your contact page.";

			return View();
		}

		public IActionResult Privacy()
		{
			return View();
		}

		[RequireAtlassianConnectJWT]
		public IActionResult JiraIndex()
		{
			var response = _connectClient.GetAsync("rest/api/latest/project").Result;
			var results = response.Content.ReadAsStringAsync().Result;

			dynamic model = new ExpandoObject();
			model.projects = results;
			return Ok(model);
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}
	}
}