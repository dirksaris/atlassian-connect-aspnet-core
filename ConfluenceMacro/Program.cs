﻿using Atlassian.Connect.AutoRegister;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;
using System.Threading.Tasks;

namespace ConfluenceMacro
{
	public class Program
	{
		public static async Task Main(string[] args)
		{
			// Configure Logging
			var loggerConfig = new LoggerConfiguration()
				.MinimumLevel.Debug()
				.MinimumLevel.Override("Microsoft", LogEventLevel.Information)
				.Enrich.FromLogContext()
				.WriteTo.Console();
			Log.Logger = loggerConfig.CreateLogger();

			var webHost = CreateWebHostBuilder(args).Build();

			// Webhost needs to be started before RegisterConnectAddon, since it requires the /install postback handler to be running
			await webHost.StartAsync();

			await webHost.RegisterConnectAddon();

			await webHost.WaitForShutdownAsync();
		}

		public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
			WebHost.CreateDefaultBuilder(args)
				.UseSerilog()
				.ConfigureLogging((context, logging) =>
				{
					logging.ClearProviders();
					logging.AddSerilog();
				})
				.UseStartup<Startup>();
	}
}
