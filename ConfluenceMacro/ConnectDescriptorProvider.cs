﻿using Atlassian.Connect;
using Atlassian.Connect.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace ConfluenceMacro
{
	public class ConnectDescriptorProvider : IConnectDescriptorProvider
	{
		private readonly IHostingEnvironment _env;
		private readonly HttpContext _httpContext;

		public ConnectDescriptorProvider(IHostingEnvironment env, IHttpContextAccessor httpContextAccessor)
		{
			_env = env;
			_httpContext = httpContextAccessor?.HttpContext;
		}

		public ConnectDescriptor GetConnectDescriptor()
		{
			return new ConnectDescriptor()
			{

				Name = "Hello World Confluence Macro",
				Description = "Atlassian Connect Confluence Static Content Macro Sample",
				Vendor = new Vendor()
				{
					Name = "Example, Inc.",
					Url = new System.Uri("http://example.com")
				},
				Authentication = new Authentication()
				{
					Type = AuthenticationType.Jwt
				},
				Scopes = new List<ScopeEnum> { ScopeEnum.Read },
				Modules = new Modules
				{
					StaticContentMacros = new StaticContentMacro[]
						{
							new StaticContentMacro
							{
								Url = "/v1/backgroundColor?color={color}&pageId={page.id}&pageVersion={page.version}&macroId={macro.id}",
								Description = new I18nProperty
								{
									Value = "Allow users to add a background colour around selected content."
								},
								Categories = new[] { "formatting" },
								OutputType = StaticContentMacroOutputType.Inline,
								BodyType = StaticContentMacroBodyType.RichText,
								Name = new I18nProperty { Value = "Background Colour (Connect) 2" },
								Key = "bg-color-macro-2",
								Parameters = new Parameter[]
								{
									new Parameter
									{
										Identifier = "color",
										Name = new I18nProperty { Value = "Color" },
										Type = "string",
										Required = true,
										Multiple = false
									}
								}
							}
						}
				}

			};
		}
	}
}
