﻿using Atlassian.Connect;
using Atlassian.Connect.Jwt;
using ConfluenceMacro.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
using System.Threading.Tasks;

namespace ConfluenceMacro.Controllers
{
	public class HomeController : Controller
	{
		private readonly ConnectClient _client;
		private readonly AtlassianConnectOptions _options;
		public HomeController(ConnectClient client, AtlassianConnectOptions options)
		{
			_client = client;
			_options = options;
		}
		public IActionResult Index()
		{
			return View();
		}

		public IActionResult About()
		{
			ViewData["Message"] = "Your application description page.";

			return View();
		}

		public IActionResult Contact()
		{
			ViewData["Message"] = "Your contact page.";

			return View();
		}

		public IActionResult Privacy()
		{
			return View();
		}

		[RequireAtlassianConnectJWT]
		public async Task<IActionResult> BackgroundColorAsync(string color, string pageId, string pageVersion, string macroId)
		{
			var url = $"/wiki/rest/api/content/{pageId}/history/{pageVersion}/macro/id/{macroId}";

			var clientReponse = await _client.GetAsync(url);

			if ((int)clientReponse.StatusCode < 200 || (int)clientReponse.StatusCode > 299)
			{
				return Content($"<strong>An error has occurred {(int)clientReponse.StatusCode} </strong>");
			}

			string responseBody = await clientReponse.Content.ReadAsStringAsync();
			var responseJObject = (JObject)JsonConvert.DeserializeObject(responseBody);
			var bodyString = responseJObject["body"].Value<string>();

			var htmlString = $"<div style=\"background-color: {color}; \">{bodyString} </ div > ";

			return Content(htmlString);
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}
	}
}
